#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

int T,n,c;
long long e, p;

int PowerMod(long a, long b, long c)
{
    int ans = 1;
    a = a % c;
    while(b>0) {
        if(b % 2 == 1)
        ans = (ans * a) % c;
        b = b/2;
        a = (a * a) % c;
    }
    return ans;
}


//遞迴函數
vector<int> battle(vector<int> v){

	//如果v的大小為0或1就不用比較直接回傳
	if(v.size()==0 || v.size()==1) return v;

	int pivot = v[v.size()/2];	//取中間點為pivot
	vector<int> winner(0);	
	vector<int> loser(0);	
	for(int i=0; i<v.size(); i++){
		if(i==v.size()/2) continue;	//遇到pivot自己則跳過
		int part = PowerMod((v[i]+pivot),e,p);
		long temp = c*(v[i]-pivot)*part;
		while(temp<0) temp+=(100*p); //如果temp<0要先變成正的(題目)
		if(temp%p > p/2.0)
			winner.push_back(v[i]); //打贏pivot去winner(會被放在左邊)
		else 
			loser.push_back(v[i]);  //打輸pivot去loser(會被放在右邊)
	}

	vector<int> combine(0);
	//呼叫遞迴
	vector<int> left = battle(winner);
	vector<int> right = battle(loser);

	//把整理好的vector放進combine裡，按照left>pivot>right的順序
	if(left.size()!=0){
		for(int i=0; i<left.size(); i++) combine.push_back(left[i]);
	}
	combine.push_back(pivot);
	if(right.size()!=0){
		for(int i=0; i<right.size(); i++) combine.push_back(right[i]);
	}

	return combine;
}

int main() {
	
	cin>>T; //輸入執行次數
	vector<vector<int>> inputs(0);
	
	//讓inputs紀錄每次輸入
	for(int i=0; i<T; i++){
		cin>>n>>c>>e>>p;

		vector<int> input(0);
		input.push_back(n);
		input.push_back(c);
		input.push_back(e);
		input.push_back(p);

		inputs.push_back(input);
	}

	for(int i=0; i<T; i++){
		//重設每次的n c e p值
		n = inputs[i][0];
		c = inputs[i][1];
		e = inputs[i][2];
		p = inputs[i][3];

		//將vector v塞入1,2,3...n
		vector<int> v(0);
		for(int a=1; a<=n; a++) v.push_back(a);
		
		//呼叫遞迴函數並將結果傳入result
		vector<int> result = battle(v);
		
		//輸出result
		for(int b=0; b<result.size(); b++) cout<<result[b]<<" ";
		cout<<endl;
	}
	system("pause");
	return 0;
}