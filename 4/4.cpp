#include <iostream>
using namespace std;

long long dp[20][6000][19][19]; //dp[digits][%7, range between 999*3~ -999*3][num_of_seven][num_of_four]
bool calc[20][6000][19][19];

int digits[20];
int digit_cnt;

bool checkLucky(long long test){
//	int ori = test;
	int num4 = 0;
	int num7 = 0;
	long long n = 0;
	while(test>0){
		n=test % 10;
		test=test / 10;
		if (n == 7) num7++;
		else if (n == 4) num4++;
	}
	if(num7>=3 && num7>num4){
//		cout<<"lucky = "<<ori<<endl;
		return true;
	}
	else return false;
}

// The last argument (eq) is used to avoid going over E
long long F(int digit, int mod, int num_seven, int num_four, bool eq) {
    // Base cases
    if (!eq && calc[digit][mod+2997][num_seven][num_four]) return dp[digit][mod+2997][num_seven][num_four];
	if (digit == 0){
		if(mod<0) mod = mod*(-1);
		if(mod%7==0 && num_seven>=3 && num_seven>num_four) return 1;
		else return 0;
	}

    long long resp = 0;

    // Test all possibilities for the next digit
    for (int d = 0; d < 10; d++) {
        if (!eq || digits[digit-1] > d) {
			int mod_change = 0;
			switch(digit % 6){
				case 1: mod_change = d;		break;
				case 2: mod_change = 10*d;	break;
				case 3: mod_change = 100*d;	break;
				case 4: mod_change = (-1)*d;		break;
				case 5: mod_change = (-1)*10*d;	break;
				case 0: mod_change = (-1)*100*d;	break;
			}
			int seven_change = 0;
			if(d==7) seven_change = 1;
			int four_change = 0;
			if(d==4) four_change = 1;

			resp += F(digit-1, mod+mod_change, num_seven+seven_change, num_four+four_change, false);
        }

        else if (digits[digit-1] == d) {
			int mod_change = 0;
			switch(digit % 6){
				case 1: mod_change = d;		break;
				case 2: mod_change = 10*d;	break;
				case 3: mod_change = 100*d;	break;
				case 4: mod_change = (-1)*d;		break;
				case 5: mod_change = (-1)*10*d;	break;
				case 0: mod_change = (-1)*100*d;	break;
			}
			int seven_change = 0;
			if(d==7) seven_change = 1;
			int four_change = 0;
			if(d==4) four_change = 1;

			resp += F(digit-1, mod+mod_change, num_seven+seven_change, num_four+four_change, true);
        }
        else break;
    }

    // Note that computations that have eq set to true
    // can't be used in other calculations of F(), as they depend on E.
    if (!eq) {
        calc[digit][mod+2997][num_seven][num_four] = true;
        dp[digit][mod+2997][num_seven][num_four] = resp;
    }

    return resp;
}

long long lucky(long long E) {
    long long tE = E;
    digit_cnt = 0;

    while (tE) {
        digits[digit_cnt++] = tE % 10;
        tE /= 10;
    }

    return F(digit_cnt, 0, 0, 0, true);
}

int main(){

	long T;
	cin>>T;
	long long l, r;
	long long total = 0;
	for(int i=0; i<T; i++){
		cin>>l>>r;
		if(checkLucky(l)) total = lucky(r) - lucky(l)+1;
		else total = lucky(r) - lucky(l);
		cout<<total<<endl;
	}
	system("pause");
	return 0;
}

